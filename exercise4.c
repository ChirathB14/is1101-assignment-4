/** this program finds factors of a given number 
 */
#include <stdio.h>
int main()
{
    int input; //variable to store the users input

    //prompting for users input
    printf("Enter a number : ");
    scanf("%d", &input);

    printf("Factors of  %d : ", input);

    //this loop will iterate until the entered number is reached
    for (int i = input; i > 0; i--)
    {
        /*
         * this if block prints the current number, if 
         * the current number is a factor
         */
        if (input % i == 0)
        {
            printf("%d ", i);
        }
        
    }

    return 0;
}