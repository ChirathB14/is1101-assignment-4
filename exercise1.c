/** This program adds the total of the numbers you have entered. 
 *  Entering a negative number or 0 will terminate the progrma otherwise
 *  the program will prompt for an input
 */
#include <stdio.h>
int main()
{
    int input = 0; // variable to store the user's input
    int total = 0; // variable to store the total value

    //this line prints a decription of program to user
    printf("\nThis program adds the total of the numbers you have entered."
            "To exit from the program enter a negative number or 0\n");

    /*
     *  Prompting value from the user before entering to loop to
     *  set the value for the input 
     */
    printf("\nEnter a number : ");
    scanf("%d", &input);

    //this while loop will iterate until input is not a positive integer
    while ( input > 0 )
    {
        //adding input value to total
        total += input;

        //prompting for input from the user
        printf("Enter a number : ");
        scanf("%d", &input);
    }
    
    //printing the total value
    printf("Total value of the numbers you have entered : %d\n", total);

    return 0;
}