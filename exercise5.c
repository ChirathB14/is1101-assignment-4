/** this program prints multiplication tables from 1 to given integer value.
 *  Integer value is taken as an user input
 */
#include <stdio.h>
#define RANGE 10 // this constatnt define the range of multiplication table
int main()
{
    int input = 0; // variable to store the  users input
    
    //prompting users input
    printf("Input a number (Multiplication will be printed"
            "from 1 to entered number) : ");
    scanf("%d", &input);

    /* this for loop control the number of multiplication tables.
     * multiplication tables from 1 to given number is genrerated through this loop
     */
    for (int i = 1; i <= input; i++)
    {
        /* this for loop prints a single multiplication for the current number
         * in the range of specified with the RANGE constant
         */
        for (int  j = 1; j <= RANGE; j++)
        {
            printf("%2d x %2d = %2d\n", i, j, i * j); // 2d used to format the output
        }

        printf("\n");
    }

    return 0;
}

/**By replacing the below code in line number range 18 - 29,
 * multiplication tables can be printed side by side. But for 
 * larger numbers multiplication tables overlap each other.
 * //////////////////////////////////////////////////////////////////////////////////
 * for (int  j = 1; j <= RANGE; j++)
 * {
 *      for (int k = 1; k <= input; k++)
 *      {
 *          printf("%2d x %2d = %2d\t", k, j, k * j); // 2d used to format the output
 *      }
 *           
 *      printf("\n");
 * }
 * /////////////////////////////////////////////////////////////////////////////////
 */       