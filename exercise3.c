/** This program reverses a number entered by user
 */
#include<stdio.h>
int main()
{
    int input = 0;
    int reversed = 0; // variable to store reversed number

    //prompting for users input
    printf("Enter a number: ");
    scanf("%d", &input);

    //this loop reverses the entered number using an arithmetic opeartion.
    while (input != 0)
    {
        int remainder = input % 10;
        reversed = reversed * 10 + remainder;
        input /= 10;
    }

    printf("Reversed number = %d", reversed);

    return 0;
}