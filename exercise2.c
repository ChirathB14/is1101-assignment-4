/** This program check an entered number is a prime number or not.
 */
#include<stdio.h>
int main ()
{
    int input;
    int count; //declaring a variable to store no of factors

    printf("Enter a number : ");
    scanf("%d", &input);

    //this loop will iterate until the entered number is reached
    for (int i = input; i > 0; i--)
    {
        /*
         * this if block increments the count variable if the current number is a factor
         */
        if (input % i == 0)
        {
            count++;
        }
        
    }

    /*
     * this block will be executed if there is more than 2 factors or 
     * entered number is 0 or 1
     */
    if (count>2 || input == 1 || input == 0)
    {
        printf("%d is not a prime number\n", input, count);
    }
    else
        {
            printf("%d is a prime number\n", input, count);
        }

    return 0;
}